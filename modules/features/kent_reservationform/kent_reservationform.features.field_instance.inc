<?php
/**
 * @file
 * kent_reservationform.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function kent_reservationform_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'entityform-reservation-field_datum'.
  $field_instances['entityform-reservation-field_datum'] = array(
    'bundle' => 'reservation',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'date',
        'settings' => array(
          'field_formatter_class' => '',
          'format_type' => 'long',
          'fromto' => 'both',
          'multiple_from' => '',
          'multiple_number' => '',
          'multiple_to' => '',
          'show_remaining_days' => FALSE,
        ),
        'type' => 'date_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'entityform',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_datum',
    'label' => 'Datum',
    'required' => 1,
    'settings' => array(
      'default_value' => 'now',
      'default_value2' => 'same',
      'default_value_code' => '',
      'default_value_code2' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'date',
      'settings' => array(
        'increment' => 15,
        'input_format' => 'd.m.Y - H:i:s',
        'input_format_custom' => '',
        'label_position' => 'above',
        'no_fieldset' => 0,
        'text_parts' => array(),
        'year_range' => '-0:+1',
      ),
      'type' => 'date_popup',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'entityform-reservation-field_e_mail_adresse'.
  $field_instances['entityform-reservation-field_e_mail_adresse'] = array(
    'bundle' => 'reservation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'email',
        'settings' => array(
          'field_formatter_class' => '',
        ),
        'type' => 'email_default',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'entityform',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_e_mail_adresse',
    'label' => 'E-mail Adresse',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'email',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'email_textfield',
      'weight' => 6,
    ),
  );

  // Exported field_instance:
  // 'entityform-reservation-field_haben_sie_besondere_w_nsch'.
  $field_instances['entityform-reservation-field_haben_sie_besondere_w_nsch'] = array(
    'bundle' => 'reservation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(
          'field_formatter_class' => '',
        ),
        'type' => 'text_default',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'entityform',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_haben_sie_besondere_w_nsch',
    'label' => 'Haben Sie besondere Wünsche?',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 8,
    ),
  );

  // Exported field_instance: 'entityform-reservation-field_lokal'.
  $field_instances['entityform-reservation-field_lokal'] = array(
    'bundle' => 'reservation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(
          'field_formatter_class' => '',
        ),
        'type' => 'list_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'entityform',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_lokal',
    'label' => 'Lokal',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'entityform-reservation-field_name_firma'.
  $field_instances['entityform-reservation-field_name_firma'] = array(
    'bundle' => 'reservation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(
          'field_formatter_class' => '',
        ),
        'type' => 'text_default',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'entityform',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_name_firma',
    'label' => 'Name / Firma',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'entityform-reservation-field_personen'.
  $field_instances['entityform-reservation-field_personen'] = array(
    'bundle' => 'reservation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(
          'field_formatter_class' => '',
        ),
        'type' => 'list_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'entityform',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_personen',
    'label' => 'Personen',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'entityform-reservation-field_raucher'.
  $field_instances['entityform-reservation-field_raucher'] = array(
    'bundle' => 'reservation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(
          'field_formatter_class' => '',
        ),
        'type' => 'list_default',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'entityform',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_raucher',
    'label' => 'Raucher',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'entityform-reservation-field_telefon_nr'.
  $field_instances['entityform-reservation-field_telefon_nr'] = array(
    'bundle' => 'reservation',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(
          'field_formatter_class' => '',
        ),
        'type' => 'text_default',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'entityform',
    'fences_wrapper' => 'div_div_div',
    'field_name' => 'field_telefon_nr',
    'label' => 'Telefon-Nr',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 7,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Datum');
  t('E-mail Adresse');
  t('Haben Sie besondere Wünsche?');
  t('Lokal');
  t('Name / Firma');
  t('Personen');
  t('Raucher');
  t('Telefon-Nr');

  return $field_instances;
}
