<?php
/**
 * @file
 * kent_page.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function kent_page_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-kent-14.
  $menus['menu-kent-14'] = array(
    'menu_name' => 'menu-kent-14',
    'title' => 'Kent 14',
    'description' => '',
  );
  // Exported menu: menu-kent-15.
  $menus['menu-kent-15'] = array(
    'menu_name' => 'menu-kent-15',
    'title' => 'Kent 15',
    'description' => '',
  );
  // Exported menu: menu-kent-16.
  $menus['menu-kent-16'] = array(
    'menu_name' => 'menu-kent-16',
    'title' => 'Kent 16',
    'description' => '',
  );
  // Exported menu: menu-kent-20.
  $menus['menu-kent-20'] = array(
    'menu_name' => 'menu-kent-20',
    'title' => 'Kent 20',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Kent 14');
  t('Kent 15');
  t('Kent 16');
  t('Kent 20');

  return $menus;
}
