<?php
/**
 * @file
 * kent_landingrestaurant.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function kent_landingrestaurant_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function kent_landingrestaurant_node_info() {
  $items = array(
    'landingrestaurant' => array(
      'name' => t('Landingrestaurant'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Titel'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
