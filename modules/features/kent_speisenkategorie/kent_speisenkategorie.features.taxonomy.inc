<?php
/**
 * @file
 * kent_speisenkategorie.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function kent_speisenkategorie_taxonomy_default_vocabularies() {
  return array(
    'speisekarte_kategorie' => array(
      'name' => 'Speisekarte Kategorie',
      'machine_name' => 'speisekarte_kategorie',
      'description' => 'Kategorien für die Speisekarte',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 4,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
