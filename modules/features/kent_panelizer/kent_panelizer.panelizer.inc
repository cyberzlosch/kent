<?php
/**
 * @file
 * kent_panelizer.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function kent_panelizer_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:homepage:default';
  $panelizer->title = 'Standard';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'homepage';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'homepage';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'slideshow' => NULL,
      'image_navigation' => NULL,
      'introtext' => NULL,
      'opening' => NULL,
      'videocont' => NULL,
      'openingtitle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'de66fe0f-f78f-4318-b0de-9f56baec3450';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-23fe0b56-42f7-42db-8d48-f88499caf9b6';
    $pane->panel = 'introtext';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '23fe0b56-42f7-42db-8d48-f88499caf9b6';
    $display->content['new-23fe0b56-42f7-42db-8d48-f88499caf9b6'] = $pane;
    $display->panels['introtext'][0] = 'new-23fe0b56-42f7-42db-8d48-f88499caf9b6';
    $pane = new stdClass();
    $pane->pid = 'new-7ade85e0-21c1-4057-8863-bfaefb2672e7';
    $pane->panel = 'opening';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_home_opening';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '7ade85e0-21c1-4057-8863-bfaefb2672e7';
    $display->content['new-7ade85e0-21c1-4057-8863-bfaefb2672e7'] = $pane;
    $display->panels['opening'][0] = 'new-7ade85e0-21c1-4057-8863-bfaefb2672e7';
    $pane = new stdClass();
    $pane->pid = 'new-724989e4-bbd5-45f7-8afa-d8e0a952774a';
    $pane->panel = 'openingtitle';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field__ffungszeiten_titel';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '724989e4-bbd5-45f7-8afa-d8e0a952774a';
    $display->content['new-724989e4-bbd5-45f7-8afa-d8e0a952774a'] = $pane;
    $display->panels['openingtitle'][0] = 'new-724989e4-bbd5-45f7-8afa-d8e0a952774a';
    $pane = new stdClass();
    $pane->pid = 'new-cb995ed7-5709-4738-8a44-48e1c6f5d460';
    $pane->panel = 'slideshow';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_restaurant_slideshow';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'field_collection_fields',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'view_mode' => 'full',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'cb995ed7-5709-4738-8a44-48e1c6f5d460';
    $display->content['new-cb995ed7-5709-4738-8a44-48e1c6f5d460'] = $pane;
    $display->panels['slideshow'][0] = 'new-cb995ed7-5709-4738-8a44-48e1c6f5d460';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-23fe0b56-42f7-42db-8d48-f88499caf9b6';
  $panelizer->display = $display;
  $export['node:homepage:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:landingrestaurant:default';
  $panelizer->title = 'Standard';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'landingrestaurant';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'landingpage';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'slideshow' => NULL,
      'herotitle' => NULL,
      'locations_top' => NULL,
      'locations_top_1' => NULL,
      'locations_top_2' => NULL,
      'locations_top_3' => NULL,
      'introduction' => NULL,
      'locations_bottom' => NULL,
      'videocont' => NULL,
      'videotitle' => NULL,
    ),
    'locations_top' => array(
      'style' => 'naked',
    ),
    'locations_top_1' => array(
      'style' => 'naked',
    ),
    'locations_top_2' => array(
      'style' => 'naked',
    ),
    'locations_top_3' => array(
      'style' => 'naked',
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'b8a4913d-4575-43be-a74e-69c1c83dda5d';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-b0161409-5be4-472c-bd5a-891d35e6d972';
    $pane->panel = 'herotitle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Kent Willkommenstext',
      'title' => '',
      'body' => '<h2>Willkommen im KENT</h2><h4>Türkisches Restaurant</h4>',
      'format' => 'editor',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b0161409-5be4-472c-bd5a-891d35e6d972';
    $display->content['new-b0161409-5be4-472c-bd5a-891d35e6d972'] = $pane;
    $display->panels['herotitle'][0] = 'new-b0161409-5be4-472c-bd5a-891d35e6d972';
    $pane = new stdClass();
    $pane->pid = 'new-bc9d2236-012a-4737-b64a-9803a9a76650';
    $pane->panel = 'locations_bottom';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_restaurant_images';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'field_collection_fields',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'view_mode' => 'full',
      ),
      'context' => 'panelizer',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'bc9d2236-012a-4737-b64a-9803a9a76650';
    $display->content['new-bc9d2236-012a-4737-b64a-9803a9a76650'] = $pane;
    $display->panels['locations_bottom'][0] = 'new-bc9d2236-012a-4737-b64a-9803a9a76650';
    $pane = new stdClass();
    $pane->pid = 'new-62df5ce6-4a1e-454e-9106-0b6c275f147c';
    $pane->panel = 'locations_top';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_restaurants';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'field_collection_view',
      'delta_limit' => '1',
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'formatter_settings' => array(
        'edit' => 'Bearbeiten',
        'delete' => 'Löschen',
        'add' => 'Hinzufügen',
        'description' => 1,
        'view_mode' => 'full',
      ),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '62df5ce6-4a1e-454e-9106-0b6c275f147c';
    $display->content['new-62df5ce6-4a1e-454e-9106-0b6c275f147c'] = $pane;
    $display->panels['locations_top'][0] = 'new-62df5ce6-4a1e-454e-9106-0b6c275f147c';
    $pane = new stdClass();
    $pane->pid = 'new-a6a2380b-1edb-4302-839d-999b01dbbbd2';
    $pane->panel = 'locations_top_1';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_restaurants';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'field_collection_view',
      'delta_limit' => '1',
      'delta_offset' => '1',
      'delta_reversed' => 0,
      'formatter_settings' => array(
        'edit' => 'Bearbeiten',
        'delete' => 'Löschen',
        'add' => 'Hinzufügen',
        'description' => 1,
        'view_mode' => 'full',
      ),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'a6a2380b-1edb-4302-839d-999b01dbbbd2';
    $display->content['new-a6a2380b-1edb-4302-839d-999b01dbbbd2'] = $pane;
    $display->panels['locations_top_1'][0] = 'new-a6a2380b-1edb-4302-839d-999b01dbbbd2';
    $pane = new stdClass();
    $pane->pid = 'new-21ff1aee-fb85-46b7-827e-42522d9c4dd7';
    $pane->panel = 'locations_top_2';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_restaurants';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'field_collection_view',
      'delta_limit' => '1',
      'delta_offset' => '2',
      'delta_reversed' => 0,
      'formatter_settings' => array(
        'edit' => 'Bearbeiten',
        'delete' => 'Löschen',
        'add' => 'Hinzufügen',
        'description' => 1,
        'view_mode' => 'full',
      ),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '21ff1aee-fb85-46b7-827e-42522d9c4dd7';
    $display->content['new-21ff1aee-fb85-46b7-827e-42522d9c4dd7'] = $pane;
    $display->panels['locations_top_2'][0] = 'new-21ff1aee-fb85-46b7-827e-42522d9c4dd7';
    $pane = new stdClass();
    $pane->pid = 'new-e4613897-4791-43e4-ac25-b4414b03cecd';
    $pane->panel = 'locations_top_3';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_restaurants';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'field_collection_view',
      'delta_limit' => '1',
      'delta_offset' => '3',
      'delta_reversed' => 0,
      'formatter_settings' => array(
        'edit' => 'Bearbeiten',
        'delete' => 'Löschen',
        'add' => 'Hinzufügen',
        'description' => 1,
        'view_mode' => 'full',
      ),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'naked',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'e4613897-4791-43e4-ac25-b4414b03cecd';
    $display->content['new-e4613897-4791-43e4-ac25-b4414b03cecd'] = $pane;
    $display->panels['locations_top_3'][0] = 'new-e4613897-4791-43e4-ac25-b4414b03cecd';
    $pane = new stdClass();
    $pane->pid = 'new-68fba9cf-c324-4990-889d-d940ac911123';
    $pane->panel = 'videocont';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_restaurant_videos';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'field_collection_view',
      'delta_limit' => '1',
      'delta_offset' => '1',
      'delta_reversed' => 0,
      'formatter_settings' => array(
        'view_mode' => 'full',
        'edit' => 'Bearbeiten',
        'delete' => 'Löschen',
        'add' => 'Hinzufügen',
        'description' => 1,
      ),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '68fba9cf-c324-4990-889d-d940ac911123';
    $display->content['new-68fba9cf-c324-4990-889d-d940ac911123'] = $pane;
    $display->panels['videocont'][0] = 'new-68fba9cf-c324-4990-889d-d940ac911123';
    $pane = new stdClass();
    $pane->pid = 'new-34284b2a-818b-4882-8580-78370b229676';
    $pane->panel = 'videocont';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_restaurant_videos';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'field_collection_view',
      'delta_limit' => '1',
      'delta_offset' => '2',
      'delta_reversed' => 0,
      'formatter_settings' => array(
        'edit' => 'Bearbeiten',
        'delete' => 'Löschen',
        'add' => 'Hinzufügen',
        'description' => 1,
        'view_mode' => 'full',
      ),
      'context' => 'panelizer',
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '34284b2a-818b-4882-8580-78370b229676';
    $display->content['new-34284b2a-818b-4882-8580-78370b229676'] = $pane;
    $display->panels['videocont'][1] = 'new-34284b2a-818b-4882-8580-78370b229676';
    $pane = new stdClass();
    $pane->pid = 'new-58594e6c-c390-4787-a65c-e5d55edc66cc';
    $pane->panel = 'videocont';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_restaurant_videos';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'field_collection_view',
      'delta_limit' => '1',
      'delta_offset' => '3',
      'delta_reversed' => 0,
      'formatter_settings' => array(
        'edit' => 'Bearbeiten',
        'delete' => 'Löschen',
        'add' => 'Hinzufügen',
        'description' => 1,
        'view_mode' => 'full',
      ),
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '58594e6c-c390-4787-a65c-e5d55edc66cc';
    $display->content['new-58594e6c-c390-4787-a65c-e5d55edc66cc'] = $pane;
    $display->panels['videocont'][2] = 'new-58594e6c-c390-4787-a65c-e5d55edc66cc';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:landingrestaurant:default'] = $panelizer;

  return $export;
}
