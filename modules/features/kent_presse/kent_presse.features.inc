<?php
/**
 * @file
 * kent_presse.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function kent_presse_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function kent_presse_node_info() {
  $items = array(
    'presse' => array(
      'name' => t('Presse'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
