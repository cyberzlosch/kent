<?php
/**
 * @file
 * kent_menu.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function kent_menu_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function kent_menu_node_info() {
  $items = array(
    'speisekarte' => array(
      'name' => t('Speisekarte'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
