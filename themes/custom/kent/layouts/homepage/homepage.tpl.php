<div class="content full-height hero-content" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

    <!-- slideshow -->
    <?php print $content['slideshow'];?>
    <!-- slideshow end -->

    <!-- hero title -->
    <div class="hero-title-holder">
        <div class="overlay"></div>


        <!-- txt-introduction-index -->

        <div class="txt-introduction">
            <div class="container">
                <div class="row">

                    <div class="txt-introduction">
                        <!--- optional! -->
                    </div>

                </div>
            </div>
        </div>

        <!-- txt-introduction-index end -->



        <!-- hero infos-index -->
        <div class="hero-infos-index">
            <div class="container">
                <div class="row-infos-index">



                    <!-- PANELS HOME -->
                    <div class="panels_home">

                        <?php
                        global $language ;
                        $lang_name = $language->language ;
                        ?>
                        <a href="/<?php print request_path();?>/about">
                            <div class="panel_item">

                                <div class="item_inner">
                                    <div class="img_item"><img src="/sites/default/files/home_15/bild7.jpg"></div>

                                    <h3><?php if ($lang_name == 'de'): print t('Über uns'); else: print t('About'); endif;?></h3>
                                </div>
                            </div>
                        </a>

                        <a href="/<?php print request_path();?>/menu">
                            <div class="panel_item">

                                <div class="item_inner">
                                    <div class="img_item"><img src="/sites/default/files/home_15/bild9.jpg"></div>
                                    <h3><?php if ($lang_name == 'de'): print t('Speisekarte'); else: print t('Menu'); endif;?></h3>
                                </div>
                            </div>
                        </a>


                        <a href="/<?php print request_path();?>/catering">
                            <div class="panel_item">

                                <div class="item_inner">
                                    <div class="img_item"><img src="/sites/default/files/home_15/bild10.jpg"></div>
                                    <h3>Catering</h3>
                                </div>
                            </div>
                        </a>


                        <a href="#">
                            <div class="panel_item">

                                <div class="item_inner">
                                    <div class="img_item"><img src="/sites/default/files/home_15/bild4.jpg"></div>
                                    <h3><?php if ($lang_name == 'de'): print t('Online-Zustellung'); else: print t('Online-Delivery'); endif;?></h3>
                                </div>
                            </div>
                        </a>

                    </div>
                    <!-- END PANELS HOME -->



                </div>
            </div>
        </div>
        <!-- end hero infos-index -->



    </div>
    <!-- hero title end -->


</div>

<div class="content" id="index-filiale">


    <!-- section events -->
    <section class="parallax-section events" id="events">

        <div class="bg bg-parallax" style="background-image:url(/sites/default/files/bg/63.jpg)" data-top-bottom="transform: translateY(300px);" data-bottom-top="transform: translateY(-300px);"></div>
        <div class="overlay"></div>

        <div class="container">
            <div class="row">

                <div class="col-md-12">


                    <div class="section-title">
                       <?php print $content['introtext'];?>
                    </div>


                </div>

            </div>
        </div>

    </section>
    <!--section events end-->


    <!-- section open -->
    <section class="parallax-section" id="open">

        <div class="bg bg-parallax" style="background-image:url(/sites/default/files/bg/2.jpg)" data-top-bottom="transform: translateY(300px);" data-bottom-top="transform: translateY(-300px);"></div>
        <div class="overlay"></div>

        <div class="container">
            <div class="row">

                <div class="col-md-12">

                    <div class="section-title">
                        <h2><?php print t('Our Openings');?></h2>

                       <?php print $content['openingtitle'];?>
                    </div>

                    <div class="clear"></div>


                    <div class="intro-center">

                        <!-- times open -->
                        <div class="times-open">
                          <?php print $content['opening'];?>
                        </div>
                        <!-- times open end  -->

                    </div>

                </div>
            </div>

        </div>
    </section>
    <!--section open end-->



</div>
<!--content end-->