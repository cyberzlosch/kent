<?php

// Plugin definition
$plugin = array(
    'title' => t('homepage'),
    'category' => t('Custom'),
    'icon' => 'homepage.png',
    'theme' => 'homepage',
    'css' => 'homepage.css',
    'regions' => array(
        'slideshow' => t('Slideshow'),
        'image_navigation' => t('Image Navigation'),
        'introtext' => t('Intro Text'),
        'openingtitle' => t('Opening Title'),
        'opening' => t('Öffnungszeiten'),
        'videocont' => t('Video Content')
    ),
);
