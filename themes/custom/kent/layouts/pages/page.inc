<?php

// Plugin definition
$plugin = array(
    'title' => t('pages'),
    'category' => t('Custom'),
    'icon' => 'pages.png',
    'theme' => 'pages',
    'css' => 'pages.css',
    'regions' => array(
        'parallax_top' => t('Parallax Image Top'),
        'pagetitle' => t('Page Title'),
        'pageslogan' => t('Page Slogan'),
        'body' => t('body'),
        'parallax_bottom' => t('Parallax Image Bottom'),
        'openingtitle' => t('Opening Title'),
        'opening' => t('Opening')
    ),
);
