<?php

// Plugin definition
$plugin = array(
    'title' => t('forms'),
    'category' => t('Custom'),
    'icon' => 'forms.png',
    'theme' => 'forms',
    'css' => 'forms.css',
    'regions' => array(
        'parallax_top' => t('Parallax Image Top'),
        'pagetitle' => t('Page Title'),
        'pageslogan' => t('Page Slogan'),
        'body' => t('body'),
        'sidebar' => t('sidebar'),
        'parallax_bottom' => t('Parallax Image Bottom'),
        'openingtitle' => t('Opening Title'),
        'opening' => t('Opening')
    ),
);
