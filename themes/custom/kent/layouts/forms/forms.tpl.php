<!--content -->
<div class="content"<?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>



    <!-- section start -->
    <section class="parallax-section start">
        <!-- <div class="bg bg-parallax" style="background-image:url()" data-top-bottom="transform: translateY(300px);" data-bottom-top="transform: translateY(-300px);"></div>-->
      <?php print $content['parallax_top'];?>
        <div class="overlay"></div>

        <div class="container">
            <div class="row">


                <div class="section-title intro">
                    <?php print $content['pagetitle'];?>
                    <?php print $content['pageslogan'];?>
                    <!--<h2>Über uns</h2>
                    <h4 class="decor-title">Das KENT kennt man</h4>-->
                </div>



            </div>
        </div>
    </section>
    <!--section start end-->



    <!-- section bg -->
    <section class="section-bg">
        <div class="container">
	       <div class="row">
		       <div class="content-left">
			   		<?php print $content['body'];?>
		       </div>
		       <div class="content-right">
			       <?php print $content['sidebar'];?>
		       </div>
	       </div>
        </div>
    </section>
    <!--section bg end-->





    <!-- section open -->
    <section class="parallax-section" id="open">
        <!--<div class="bg bg-parallax" style="background-image:url()" data-top-bottom="transform: translateY(300px);" data-bottom-top="transform: translateY(-300px);"></div>-->
        <?php print $content['parallax_bottom'];?>

        <div class="overlay"></div>

        <div class="container">
            <div class="row">

                <div class="col-md-12">

                    <div class="section-title">
                        <h2><?php print t('Our Openings');?></h2>

                        <?php print $content['openingtitle'];?>
                    </div>

                    <div class="clear"></div>


                    <div class="intro-center">

                        <!-- times open -->
                        <div class="times-open">

                            <?php print $content['opening'];?>

                        </div>
                        <!-- times open end  -->

                    </div>

                </div>
            </div>

        </div>
    </section>
    <!--section open end-->



</div>
<!--content end-->