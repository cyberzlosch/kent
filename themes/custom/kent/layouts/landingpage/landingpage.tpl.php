<?php
/**
 * @file
 * Template for a 2 column panel layout.
 *
 * This template provides a two column panel display layout, with
 * each column roughly equal in width.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['left']: Content in the left column.
 *   - $content['right']: Content in the right column.
 */
?>

<!-- hero-content -->
<div class="content full-height hero-content" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <!-- slideshow -->

    <?php print $content['slideshow'];?>


  <!-- end slideshow -->

  <!-- hero title -->
  <div class="hero-title-holder">
    <div class="overlay"></div>

    <div class="hero-logo-index">
      <img src="<?php echo file_create_url(drupal_get_path('theme', 'kent') . '/css/img/logo.png');?>">
    </div>


    <div class="hero-title">
      <?php print $content['herotitle'];?>
    </div>

    <!-- hero locations -->
    <div class="hero-locations">
      <div class="container">
        <div class="row">


            <?php print $content['locations_top'];?>
          


        </div>
      </div>
    </div>
    <!-- hero locations end -->

  </div>
  <!-- hero title end -->


</div>
<!-- hero-content end -->




<!--content -->
<div class="content">

  <!-- section-bg -->
  <section class="section-bg" id="locations-home">


    <div class="container">


      <div class="row">


        <div class="col-md-12">
            <?php print $content['introduction'];?>
        </div>
            <?php print $content['locations_bottom'];?>
      </div>
    </div>
  </section>
  <!--section bg end-->



  <!-- section video -->
  <section class="parallax-section">

    <div class="bg bg-parallax" style="background-image:url(/sites/default/files/bg/2.jpg)" data-top-bottom="transform: translateY(300px);" data-bottom-top="transform: translateY(-300px);"></div>
    <div class="overlay"></div>

    <div class="container">
      <div class="row">
        <div class="col-intro-video">
          <div class="preview-video-txt">
            <?php print $content['videotitle'];?>
          </div>
          </div>
          <?php print $content['videocont'];?>
      </div>
    </div>
  </section>
  <!--section video end-->


</div>
<!--content end-->

