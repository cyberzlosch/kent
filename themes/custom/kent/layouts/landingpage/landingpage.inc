<?php

// Plugin definition
$plugin = array(
  'title' => t('landingpage'),
  'category' => t('Custom'),
  'icon' => 'landingpage.png',
  'theme' => 'landingpage',
  'css' => 'landingpage.css',
  'regions' => array(
    'slideshow' => t('Slideshow'),
    'herotitle' => t('Landing Hero Title'),
    'locations_top' => t('Locations Top 1'),
    'locations_top_1' => t('Locations Top 2'),
    'locations_top_2' => t('Locations Top 3'),
    'locations_top_3' => t('Locations Top 4'),
    'introduction' => t('Intro Text'),
    'locations_bottom' => t('Locations Bottom'),
    'videotitle' => t('Video Title'),
    'videocont' => t('Video Content')
  ),
);
