<?php
/**
 * Implements hook_preprocess_field().
 */
function kent_preprocess_field(&$vars) {
    // For Field Restaurants on Landingpage
    if ($vars['element']['#field_name'] == 'field_restaurants') {
        $vars['theme_hook_suggestions'][] = 'field__restaurants';
        $field_array = array('field_restaurant_name', 'field_restaurant_adresse', 'field_restaurant_link');
        rows_from_field_collection($vars, 'field_restaurants', $field_array);
    }
    // For Field Slideshow on Landingpage
    if ($vars['element']['#field_name'] == 'field_restaurant_slideshow') {
        $vars['theme_hook_suggestions'][] = 'field__restaurantslide';
        $field_array = array('field_slide_image',);
        rows_from_field_collection($vars, 'field_restaurant_slideshow', $field_array);
    }

    // For Field Restaurant Images on Landingpage
    if ($vars['element']['#field_name'] == 'field_restaurant_images') {
        $vars['theme_hook_suggestions'][] = 'field__restaurantimages';
        $field_array = array('field_restaurant_imagelink');
        rows_from_field_collection($vars, 'field_restaurant_images', $field_array);
    }

    // For Field Restaurant Videos on Landingpage
    if ($vars['element']['#field_name'] == 'field_restaurant_videos') {
        $vars['theme_hook_suggestions'][] = 'field__restaurantvideos';
        $field_array = array('field_restaurant_video');
        rows_from_field_collection($vars, 'field_restaurant_videos', $field_array);
    }

    // For Field Page Parallax Top
    if ($vars['element']['#field_name'] == 'field_parallax_top') {
        $vars['theme_hook_suggestions'][] = 'field__parallaxtop';
        $field_array = array('field_parallax_image');
        rows_from_field_collection($vars, 'field_parallax_top', $field_array);
    }

    // For Field Page Parallax Top
    if ($vars['element']['#field_name'] == 'field_parallax_bottom') {
        $vars['theme_hook_suggestions'][] = 'field__parallaxbottom';
        $field_array = array('field_parallax_image');
        rows_from_field_collection($vars, 'field_parallax_bottom', $field_array);
    }

    // For Field Page Parallax Top
    if ($vars['element']['#field_name'] == 'field_speisen') {
        $vars['theme_hook_suggestions'][] = 'field__speisen';
        $field_array = array('field_speisen_titel','field_speisen_preis','field_speisen_beschreibung', 'field_speisen_allergene', 'field_speisen_beschreibung_2', 'field_speisen_preis_2', 'field_menge_preis_1');
        rows_from_field_collection($vars, 'field_speisen', $field_array);
    }

    // For Field Gallery Image
    if ($vars['element']['#field_name'] == 'field_gallery_bild') {
        $vars['theme_hook_suggestions'][] = 'field__galleryimage';
        $field_array = array('field_gallery_image','field_gallery_category', 'field_gallery_thumb', 'field_gallery_alt');
        rows_from_field_collection($vars, 'field_gallery_bild', $field_array);
    }

    // For Field Pressebild
    if ($vars['element']['#field_name'] == 'field_pressebild') {
        $vars['theme_hook_suggestions'][] = 'field__presseimage';
        $field_array = array('field_gallery_image','field_gallery_thumb');
        rows_from_field_collection($vars, 'field_pressebild', $field_array);
    }
}


/**
 * Creates a simple text rows array from a field collections, to be used in a
 * field_preprocess function.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 *
 * @param $field_name
 *   The name of the field being altered.
 *
 * @param $field_array
 *   Array of fields to be turned into rows in the field collection.
 */
function rows_from_field_collection(&$vars, $field_name, $field_array) {
    $vars['rows'] = array();
    foreach($vars['element']['#items'] as $key => $item) {
        $entity_id = $item['value'];
        $entity = field_collection_item_load($entity_id);
        $wrapper = entity_metadata_wrapper('field_collection_item', $entity);
        $row = array();
        foreach($field_array as $field){
            $row[$field] = field_view_field('field_collection_item', $entity, $field, 'full');
        }
        $vars['rows'][] = $row;
    }
}


function kent_preprocess_html(&$vars) {
  $viewport = array(
   '#tag' => 'meta',
   '#attributes' => array(
     'name' => 'viewport',
     'content' => 'width=device-width, initial-scale=1, maximum-scale=2, user-scalable=yes',
   ),
  );
  drupal_add_html_head($viewport, 'viewport');
}
