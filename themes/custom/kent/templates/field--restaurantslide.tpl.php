<div class="slideshow-container" data-top-bottom="transform: translateY(300px);" data-bottom-top="transform: translateY(-300px);">
    <div class="slides-container">
<?php foreach($rows as $row): ?>
    <?php if ($row['field_slide_image'][0]): ?>
    <div class="bg" style="background-image: url(<?php print render($row['field_slide_image'][0]); ?>)"></div>
    <?php endif; ?>
    <?php if ($row['field_slide_image'][1]): ?>
    <div class="bg" style="background-image: url(<?php print render($row['field_slide_image'][1]); ?>)"></div>
    <?php endif; ?>
    <?php if ($row['field_slide_image'][2]): ?>
    <div class="bg" style="background-image: url(<?php print render($row['field_slide_image'][2]); ?>)"></div>
    <?php endif; ?>
    <?php if ($row['field_slide_image'][3]): ?>
    <div class="bg" style="background-image: url(<?php print render($row['field_slide_image'][3]); ?>)"></div>
    <?php endif; ?>
    <?php if ($row['field_slide_image'][4]): ?>
    <div class="bg" style="background-image: url(<?php print render($row['field_slide_image'][4]); ?>)"></div>
    <?php endif; ?>
    <?php if ($row['field_slide_image'][5]): ?>
    <div class="bg" style="background-image: url(<?php print render($row['field_slide_image'][5]); ?>)"></div>
    <?php endif; ?>
<?php endforeach; ?>
     </div>
</div>
