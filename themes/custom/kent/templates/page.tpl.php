<div class="loader"><img src="<?php echo file_create_url(drupal_get_path('theme', 'kent') . '/css/img/loader.gif');?>" alt=""></div>
        <!-- main start -->
<div id="main">
            <!-- header -->	
            <header>
                <div class="header-inner">
                    <div class="container">
           
                        <!--logo-->             
                        <div class="logo-holder">
                            <a href="/">
                            <?php $url = drupal_get_path_alias("node/$node->nid");?>
                            <?php if (drupal_is_front_page()):?>
                                <img src="<?php echo file_create_url(drupal_get_path('theme', 'kent') . '/css/img/logo1.png');?>" class="respimg logo-vis" alt="">
                                <img src="<?php echo file_create_url(drupal_get_path('theme', 'kent') . '/css/img/logo2.png');?>" class="respimg logo-notvis" alt="">
                            <?php elseif(preg_match('/kent14/', $url)):?>
                                <img src="<?php echo file_create_url(drupal_get_path('theme', 'kent') . '/css/img/logo-kent14.png');?>" class="respimg logo-vis" alt="">
                                <img src="<?php echo file_create_url(drupal_get_path('theme', 'kent') . '/css/img/logo2-kent14.png');?>" class="respimg logo-notvis" alt="">
                            <?php elseif(preg_match('/kent15/', $url)):?>
                                <img src="<?php echo file_create_url(drupal_get_path('theme', 'kent') . '/css/img/logo-kent15.png');?>" class="respimg logo-vis" alt="">
                                <img src="<?php echo file_create_url(drupal_get_path('theme', 'kent') . '/css/img/logo2-kent15.png');?>" class="respimg logo-notvis" alt="">
                            <?php elseif(preg_match('/kent16/', $url)):?>
                                <img src="<?php echo file_create_url(drupal_get_path('theme', 'kent') . '/css/img/logo-kent16.png');?>" class="respimg logo-vis" alt="">
                                <img src="<?php echo file_create_url(drupal_get_path('theme', 'kent') . '/css/img/logo2-kent16.png');?>" class="respimg logo-notvis" alt="">
                            <?php elseif(preg_match('/kent20/', $url)):?>
                                <img src="<?php echo file_create_url(drupal_get_path('theme', 'kent') . '/css/img/logo-kent20.png');?>" class="respimg logo-vis" alt="">
                                <img src="<?php echo file_create_url(drupal_get_path('theme', 'kent') . '/css/img/logo2-kent20.png');?>" class="respimg logo-notvis" alt="">
                             <?php endif;?>

                            </a>
                        </div>

                        <div class="nav-holder">
                            <nav>
                               <?php print render ($page['mainnav']);?>
                            </nav>
                        </div>
                        <div class="nav-sm-holder"><?php print render($page['socialbar']);?><a href="https://www.facebook.com/kent1150" title="Facebook" class="icon-facebook" target="_blank"><img src="<?php echo file_create_url(drupal_get_path('theme', 'kent') . '/css/img/btn-sm.png');?>"></a><a href="http://www.austrian.photos/city/vienna/place/235077385-kent-restaurant-funfhaus.html" class="icon-instagram" title="Instagram" target="_blank"><img src="<?php echo file_create_url(drupal_get_path('theme', 'kent') . '/css/img/btn-sm.png');?>"></a> </div>
                    	<div class="nav-lang-holder"><?php print render ($page['langswitch']);?></div>
                    <?php print render($page['header']);?>
                    
                    
                  </div>
                </div>
            </header>
            <!--header end-->
            
            
            <!-- wrapper -->	
<div id="wrapper">


    <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
    <a id="main-content"></a>
    <?php print render($title_prefix); ?>
    <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
    <?php print render($page['help']); ?>
    <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
    <?php print render($page['content']); ?>
    <?php print $feed_icons; ?>


<!-- footer -->
<footer>
<!-- footer INNER -->
<div class="footer-inner">
<div class="container">
<div class="row">

<!-- FOOTER-INNER-LEFT -->
<div class="col-footer-inner">

	<div class="footer-all-kents">
		<div class="all-kents">
            <?php print render($page['footer_left']);?>

		</div>
	</div>
            

</div>
<!-- END FOOTER-INNER-LEFT -->

<!-- FOOTER-INNER-RIGHT -->
<div class="col-footer-inner">
<div class="footer-infos">
	<?php print render($page['footer_right']);?>

</div>
</div>
<!-- END FOOTER-INNER-RIGHT -->         
        
                            
</div>
                            
<div class="bold-separator"><span></span></div>
                            
                            
	
</div>
</div>
<!-- footer INNER -->

                   
<div class="to-top-holder">
	<div class="container"><div class="to-top">TOP</div></div>
</div>


</footer>
<!--footer end --> 

</div>
<!-- wrapper end -->
</div>
<!-- Main end -->