<?php foreach($rows as $row): ?>
    <div class="col-hero-locations">
        <a href="<?php print render ($row['field_restaurant_link']);?>">
            <div class="hero-locations-content">
                <h4> <?php print render($row['field_restaurant_name']); ?></h4>
                <?php
                global $language ;
                $lang_name = $language->language ;
                ?>
                <span class="txt"><?php print render($row['field_restaurant_adresse']); ?></span>
            </div>
        </a>
        <div class="delivery"><a href="#"><?php if ($lang_name=='de'): print t('Online-Zustellservice'); else: print t('Online-delivery'); endif;?></a></div>
    </div>
<?php endforeach;?>


