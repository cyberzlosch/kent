
<div class="sortfilter">
<a class="filter" data-filter="all">Show All</a>
<a class="filter" data-filter=".Catering">Catering</a>
<a class="filter" data-filter=".Events">Events</a>
<a class="filter" data-filter=".Restaurant">Restaurant</a>
<a class="filter" data-filter=".Speisen">Speisen</a>
</div>
<div id="Container" class="popup-gallery">


<?php foreach($rows as $row): ?>
    <div class="gallery-item-4 menu mix<?php print render($row['field_gallery_category']);?>">
        <div class="grid-item-holder">
            <div class="box-item">
                <a title="<?php print render($row['field_gallery_alt']);?>" href="<?php print render($row['field_gallery_image']);?>">
                    <span class="overlay"></span>
                     <?php print render ($row['field_gallery_thumb']);?>
                </a>
            </div>
        </div>
    </div>
<?php endforeach;?>
</div>
