<div class="popup-gallery">
<?php foreach($rows as $row): ?>
    <div class="gallery-item-3 menu">
        <div class="grid-item-holder">
            <div class="box-item">
                <a title="" href="<?php print render($row['field_gallery_image']);?>">
                    <span class="overlay"></span>
                    <?php print render ($row['field_gallery_thumb']);?>
                </a>
            </div>
        </div>
    </div>
<?php endforeach;?>
</div>
