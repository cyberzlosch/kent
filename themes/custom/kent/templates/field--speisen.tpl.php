
<?php foreach($rows as $row): ?>
<div class="menu-item">
    <div class="eintrag">
    <div class="title">
        <div class="txt"><span><?php print render ($row['field_speisen_titel']);?><span class="alergene"><?php print render($row['field_speisen_allergene']); ?></span></span></div>
       <div class="price"><span class="desc-price-1"><?php print render($row['field_menge_preis_1']);?></span><span><?php print render ($row['field_speisen_preis']);?> €</span></div>
    </div>
    <div class="desc">
        <div class="lang_01"><?php print render ($row['field_speisen_beschreibung']);?></div>
        <div class="price2"><span class="desc-price-2"><?php print render($row['field_speisen_beschreibung_2']);?></span><span><?php print render($row['field_speisen_preis_2']);?> </span></div>
    </div>
    </div>
</div>
<?php endforeach;?>

