<?php
/**
 * @file
 * Kent installation process.
 */

/**
 * Implements hook_install_tasks().
 */
function kent_install_tasks() {
  // Add any install tasks.
}

/**
 * Implements hook_install_tasks_alter().
 */
function kent_install_tasks_alter() {
  // Alter existing install tasks.
}