; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.

core = 7.x

; API version
; -----------
; Every makefile needs to declare its Drush Make API version. This version of
; drush make uses API version `2`.

api = 2

; Defaults
; --------
defaults[projects][subdir] = contrib

; Modules
projects[ctools][version] = 1.9
projects[ctools][patch] = "https://www.drupal.org/files/issues/ctools-offset-rendering-2456327-7.patch"
projects[magic][version] = 2.2
projects[features][version] = 2.7
projects[date][version] = 2.9
projects[email][version] = 1.3
projects[entity][version] = 1.6
projects[entityform][version] = 2.0-rc1
projects[entityreference][version] = 1.1
projects[libraries][version] = 2.2
projects[wysiwyg][version] = 2.2
projects[fences][version] = 1.2
projects[views][version] = 3.13
projects[semanticviews][version] = 1.0-rc2
projects[entityreference][version] = 1.1
projects[jquery_update][version] = 2.7
projects[panels][version] = 3.5
projects[panelizer][version] = 3.1
projects[admin_menu][version] = 3.0-rc5
projects[field_collection][version] = 1.0-beta10
projects[field_collection][patch][] = "https://www.drupal.org/files/issues/field-collection-2599248-2.patch"
projects[link][version] = 1.3
projects[youtube][version] = 1.6
projects[image_url_formatter][version] = 1.4
projects[strongarm][version] = 2.0
projects[i18n][version] = 1.13
